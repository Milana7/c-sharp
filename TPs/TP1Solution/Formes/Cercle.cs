﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formes
{
    public class Cercle : Forme
    {
        public double Rayon { get; set; }

        public override double Aire => Math.PI * Math.Pow(this.Rayon, 2);

        public override double Perimetre => 2 * Math.PI * this.Rayon;

        public override string ToString()
        {
            return string.Format("Cercle de rayon {0}", this.Rayon) + base.ToString();
        }
    }
}
