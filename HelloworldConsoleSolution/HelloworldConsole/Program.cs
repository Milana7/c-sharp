﻿using BO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Extensions;

namespace HelloworldConsole
{
    class Program
    {    
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world");

            Personne personne1 = new Personne
            {
                // Appel au setter de "Nom"
                Nom = "Dupont",
                Prenom = "Michel"
            };

            personne1.DateNaissance = new DateTime(1930, 1, 12);
            Console.WriteLine(personne1);

            // Affectation d'une référence 
            Personne personne2 = personne1;

            personne1.Nom = "Dupond";

            Console.WriteLine(personne2);

            Point point1 = new Point(10, 15);

            // Affectation d'une valeur (car Point est un struct)
            Point point2 = point1;

            point1.X = 40;

            Console.WriteLine(point2);

            List<IJouet> jouets = new List<IJouet>();

            jouets.Add(new Ballon());
            jouets.Add(new Raquette());

            foreach (IJouet jouet in jouets)
            {
                jouet.Jouer();
            }

            // Appel a une méthode d'extension
            IJouet random = jouets.Random();
            Console.WriteLine("Jouet random : {0}", random);

            string s = "Toto";

            Console.WriteLine(s.AddENIBefore());
            Console.WriteLine("Titi".AddENIBefore());

            FuncEtActions.Demo();
            LinqDemos.DoDemo();

            Console.ReadLine();
        }
    }
}
