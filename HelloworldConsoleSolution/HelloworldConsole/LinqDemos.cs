﻿using BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloworldConsole
{
    public class LinqDemos
    {
        public static void DoDemo()
        {
            List<IJouet> liste = new List<IJouet>();
            liste.Add(new Ballon { Nom = "Ballon de foot", Matiere = "Cuir", ListeDePrix = new List<double> { 15, 13 } });
            liste.Add(new Ballon { Nom = "Ballon de basket", Matiere = "Plastique", ListeDePrix = new List<double> { 20, 17 } });
            liste.Add(new Ballon { Nom = "Ballon de rugby", Matiere = "Cuir", ListeDePrix = new List<double> { 25 } });
            liste.Add(new Raquette { Nom = "Raquette de tennis", Matiere = "Plastique", ListeDePrix = new List<double> { } });

            IJouet ballonFoot = liste.First();
            ballonFoot = liste.FirstOrDefault();

            ballonFoot = liste.Where(j => j.Nom == "Ballon de foot")
                              .FirstOrDefault();

            ballonFoot = liste.FirstOrDefault(j => j.Nom == "Ballon de foot");

            ballonFoot = liste.Where(j => j.Nom.Contains("foot"))
                              .FirstOrDefault();

            Raquette raquetteTennis = liste.OfType<Raquette>()
                                           .FirstOrDefault();
            raquetteTennis = liste.LastOrDefault() as Raquette;

            raquetteTennis = liste.OrderByDescending(l => l.Nom)
                                  .FirstOrDefault() as Raquette;

            // Récupération des 2 premiers éléments
            List<IJouet> deuxPremiersElements = liste.Take(2).ToList();

            string s = string.Join(", ", deuxPremiersElements.Select(e => e.Nom));
            Console.WriteLine(s);

            List<IJouet> ballonDeRugbyToutSeul = liste.Skip(2).Take(1).ToList();

            IEnumerable<IJouet> listeTriee = liste.OrderBy(l => l.Nom).ThenBy(l => l.Matiere);
            Console.WriteLine("Liste triée = {0}", string.Join(", ", listeTriee));

            bool allAreBalloons = liste.All(l => l is Ballon);
            bool hasAnyRaquette = liste.Any(l => l is Raquette);

            Console.WriteLine("La liste contient que des ballons ? " + allAreBalloons);
            Console.WriteLine("La liste contient au moins une raquette ? " + hasAnyRaquette);

            // Le select many aggrége les sous liste en une liste finale
            IEnumerable<string> nomsListe = liste.Select(l => l.Nom);
            IEnumerable<List<double>> innerListe = liste.Select(l => l.ListeDePrix);
            IEnumerable<double> prixJouets = liste.SelectMany(l => l.ListeDePrix);

            // Les fonctions Average/Min/Max jettent des erreurs si la liste est vide
            // DefaultIfEmpty permets de gérer cette erreur avant qu'elle ne survienne
            prixJouets = prixJouets.DefaultIfEmpty();

            double moyennePrix = prixJouets.Average();

            double maximalPrix = prixJouets.Max();
            double minimalPrix = prixJouets.Min();

            Console.WriteLine("Prix moyen = {0}", moyennePrix);
            Console.WriteLine("Prix min = {0}", minimalPrix);
            Console.WriteLine("Prix max = {0}", maximalPrix);

            // Instanciation d'un type anonyme avec 2 propriétés tata et tutu
            var toto = new
            {
                tata = "titi",
                tutu = "toto"
            };

            Console.WriteLine(toto.tata); // Affiche titi
        }
    }
}
