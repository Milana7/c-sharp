﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Extensions
{
    /// <summary>
    /// Une classe d'extension doit être static
    /// </summary>
    public static class ListExtension
    {
        /// <summary>
        /// Retourne un item pris au hasard dans la liste.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="liste"></param>
        /// <returns></returns>
        public static T Random<T>(this List<T> liste)
        {
            if (liste == null)
            {
                throw new ArgumentNullException(nameof(liste));
            }


            return liste.OrderBy(i => Guid.NewGuid()).FirstOrDefault();
        }
    }
}
