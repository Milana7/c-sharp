﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO
{
    public class Personne
    {
        // Attributs privés de la classe
        private string _nom;
        private string _prenom;

        // Propriétés publiques
        public string Nom
        {
            get
            {
                return _nom;
            }
            set
            {
                _nom = value;
            }
        }

        public string Prenom
        {
            get => _prenom;
            set => _prenom = value;
        }

        public DateTime DateNaissance { get; set; }

        /// <summary>
        /// On redéfinit la méthode ToString()
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format(
                    "Personne : {0} {1}. Date de naissance : {2}",
                    this.Prenom,
                    this.Nom,
                    this.DateNaissance);
        }
    }
}
