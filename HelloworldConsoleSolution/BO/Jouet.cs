﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO
{
    public abstract class Jouet : IJouet
    {
        public string Nom { get; set; }
        public string Matiere { get; set; }
        public List<double> ListeDePrix { get; set; }
        public abstract void Jouer();

        public virtual string Couleur()
        {
            return "Bleu";
        }

        public void Lancer()
        {

        }

        public override string ToString()
        {            
            return $"\nType = { this.GetType().Name }, Nom = { this.Nom }, Matiere = { this.Matiere }";
        }
    }
}
